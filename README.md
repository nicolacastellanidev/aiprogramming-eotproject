# AIProgramming-EOTProject

### Legenda:


***CICA***: CisterninoCastellani  
***reference positon***: the position of the gameobject  
***GK***: goal keeper

## Our solution

We've decided to use **Behaviour Tree** to implement our custom AI solution for the project.

We've started migrating the entire **tnStandardAIInputFiller** into a behavior tree using ***Behaviour designer***.

We've created a custom factory called ***CicaAIFactory*** which handles the roles-per-game and the input parameters for our AI.

This core factory is extended by ***HardAIFactory*** and ***NormalAIFactory***, with different AIParams to handle different difficulty levels.

### Solution #1 - Conditional Abort

Under the branch ***development*** we have implemented a conditional abort based tree.
The actions keep running and re-evaluating ouputs instead of re-evaluating the entire tree.

This solution is pretty similar to the second, but the player are more reactive and the output management
is more precise.

### Solution #2 - Behaviour Tree constant re-evaluation

This solution is pretty similar to the first one, but the tree is evaluated at each frame.
Both solutions produces the same AI, but with different performances and lock issues on generated output.

You can try this different approach under ***feature/behaviour_tree_2nd_approach*** branch.

---

## Project structure

Our project is structured as follow:

* Nick_Nick
  * ***CICA Behavior Tree***: here there are our behaviour trees
  * ***CICA Shared Variables***: Custom defined shared variables
  * ***CICA Tasks***: Custom defined actions and conditionals
  * ***Resources***: Contains custom resources

---

## Input filler

We've decided to handle the ***input*** and ***ouput*** logic the same as standard AI, using a custom filler called ***tnBTCicaAIInputFiller***:

1. The filler fills ***static inputs*** when constructed (e.g. min and max energy consumed, cooldowns, etc..) passing target params settings to aiParams instance.

2. Each game loop, updates the ***shared input data***, to perform next actions for AI.
3. Each game loop, updates ***game timers***
4. The Behavior Tree autonomously updates its output, using a Builder style pattern to always reset and write
only the actual data (see **SharedOutputGameData** file for more information).

---
## Core classes

* ***CicaBaseAction*** - core action class, extended by each of our custom actions. Handles data stream and action executions.
* ***CicaBaseConditional*** - core conditional class, updates data and executes conditional.
* ***CicaTaskData*** - contains data and useful methods for tasks (action and conditionals).
* ***CicaActionData*** - contains specific data and methods for actions.

## Custom Actions

We've created this list of actions, some are taken directly from standard implementation, the other are custom evolution of them:

1. Attract
2. ***AttractBallInDirection*** - force attract ball in direction
3. ChargeBall
4. ***CoverGoal*** - cover goal from enemy kicks in reference position
5. ***DefensiveCharge*** - like the normal charge, but interposing ball position to team GK position
6. ***InterposeBetweenBallAndMyGoal*** - like defensive charge, but without moving forward
7. ***ProtectGoal*** - interposes between ball direction and GK position, standing in gk position
8. ***RecoverAnAdvancedPosition*** - recover in attack, looking for a goal occasion
9. RecoverBall
10. SeekAndFlee
11. ***SeekMyGoal*** - tries to reach GK position avoiding ball
12. ***ComputeAndAttractBallInDirection*** - if the ball isn't near a border, moves the ball in direction of opponent Gk
13. Tackle
14. ***WaitForBallForReferencePosition*** - instead of seek and flee, the character will wait in front of GK position to kick the ball in

## Custom Conditionals

We've created a set of conditional task to handle complex logic inside our tree:

1. ***CanAttract***
2. ***IsAbleToScore***
3. ***IsAttractionDirectionEnforced***
4. ***IsBallBehindDefenderArea***
5. ***IsBallCloseToAnyBorder***
6. ***IsBallCloseToBorder***
7. ***IsBallGoingToReachAnAlly***
8. ***IsBallInAttractRange***
9. ***IsBallInMyHalfSize***
10. ***IsBallNearToMyGoal***
11. ***IsBehindBall***
12. ***IsMyGoalPositionInFieldSide***
13. ***IsNearestToBall***
14. ***IsNearestToGoal***
15. ***IsNearestToGoalBehindBall***
16. ***IsNearestToMateChargingBall***
17. ***IsThereAnEnemyNearbyBall***
18. ***IsThereAnyMateInsideMyGkArea***
19. ***ShouldIPassMidfield***

---

## Difficulty level management

One of the goals of this exam was to handle different AI difficulty levels, trying to propose a different
solution to the current implemented.

We thought about some solutions to implement:

1. ***Behaviour tree reduction*** (logic simplification), for example using only seek and flee instead of
   advanced positioning and goal covering, or hiding some information to the player (e.g. mates positioning).

2. ***Input delays***, this is the current solution thought by the original developers. Basically this
   solution introduces a time handicap on input resolution, without changing the core logic.

3. ***Level-based AI parameters***, changing some properties like kickPrecision, or energy costs, could lead
   to a different levels of difficulty without sacrificing the core game mechanics.

We've implemented the last solution as we tough about keeping the same game feel but with some handicaps.

This solution is softer with the gameplay, and lead to greater challenges at lower levels.
In the beginning, the player will have a slow learning curve, but will gain a significant level of expertise
to handle even harder situations.

Check out the 2 params sets under ***Assets\_MasterVR2020-2021\Nick_Nick\Resources\Data\AI\\***:

1. **AIParams_Normal**
2. **AIParams_Hard**

---

## What have we learned

This exam is basically a big challenge about thinking on a *"realistic"* and *"funny"* AI to play with.

Splitting down the TuesdayNight code was an amazing challenge, looking how the team have figured out some problems and obstacles was a great occasion for learning.

Creating the tree is just the tip of the iceberg, the real challenge is to create a complete and bug-free IA, without
creating a monster at the game. And there we had our big learning moment.

Thinking only about having a good AI, and good as a precise and mathematical player, is not a good choice.
So we moved on a less-perfect AI favoring a mid-tier player with good moves (like preventing self goals or stupid dashing).

This choice lead us to a great AI (in our opinion, of course), with a great defence play and some cool trickshots.

We had a lot of fun developing it, and trying to beat it.

## Conclusions

Behaviour tree is a great choice and maybe one future choice for an AI system. Maybe is not powerful as neural networks and could lead to
expensive development costs and poor readability with large systems. But we have still a lot to learn and this system is
a great door to jump in the AI world. It also has the advantage to be really useful for thinking and creating a powerful AI system.

The final solution can be found in branch ***main***.
Another valid approach can be found under the branch ***release/behavior_tree_second_approach***.
