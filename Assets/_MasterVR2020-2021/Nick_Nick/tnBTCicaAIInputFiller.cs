﻿using BehaviorDesigner.Runtime;
using Nick_Nick.CICA_SharedVariables;
using TuesdayNights;
using UnityEditor;
using UnityEngine;

namespace Nick_Nick
{
    public class tnBTCicaAIInputFiller : tnStandardAIInputFillerBase
    {
        private BehaviorTree m_behavior_tree = null;

        private AIRole m_Role = AIRole.Null;
        
        private InputGameData m_data = new InputGameData();
        private GameTimers m_timers = new GameTimers();

        // STATIC VARIABLES
        private static string s_Params = "Data/AI/AIParams";
        
        // tnInputFiller's INTERFACE
        public tnBTCicaAIInputFiller(GameObject i_Self, AIRole i_Role, string CustomParams) : base(i_Self)
        {
            if(CustomParams != null) 
                s_Params = CustomParams;
            
            m_Role = i_Role;

            m_behavior_tree = i_Self.GetComponent<BehaviorTree>();
            
            m_behavior_tree.SetVariableValue("CharacterRole", m_Role);
            m_behavior_tree.SetVariableValue("Self", self);
            m_behavior_tree.SetVariableValue("OutputGameData", new OutputGameData());
            m_behavior_tree.SetVariableValue("InputGameData", new InputGameData());
            m_behavior_tree.SetVariableValue("GameTimers", new GameTimers());
            
            // Setup parameters.

            tnStandardAIInputFillerParams aiParams = Resources.Load<tnStandardAIInputFillerParams>(s_Params);

            if (aiParams == null)
            {
                Debug.LogWarning("AI Params is null");
                return;
            }
            
            // Seek-and-flee behaviour.

            aiParams.GetMinFleeDistanceFactor(out m_data.m_MinFleeDistanceFactor);
            aiParams.GetMaxFleeDistanceFactor(out m_data.m_MaxFleeDistanceFactor);

            // Separation.

            aiParams.GetSeparationThreshold(out m_data.m_SeparationThreshold);

            // Energy thresholds.

            aiParams.GetMinDashEnergy(out m_data.m_MinDashEnergy);
            aiParams.GetMinKickEnergy(out m_data.m_MinKickEnergy);
            aiParams.GetMinTackleEnergy(out m_data.m_MinTackleEnergy);
            aiParams.GetMinAttractEnergy(out m_data.m_MinAttractEnergy);

            // Cooldown timers.

            aiParams.GetDashCooldown(out m_data.m_DashCooldown);
            aiParams.GetKickCooldown(out m_data.m_KickCooldown);
            aiParams.GetTackleCooldown(out m_data.m_TackleCooldown);
            aiParams.GetAttractCooldown(out m_data.m_AttractCooldown);

            // Dash behaviour.

            aiParams.GetDashDistance(out m_data.m_DashDistance);
            aiParams.GetForcedDashDistance(out m_data.m_ForcedDashDistance);

            // Kick behaviour.

            aiParams.GetKickPrecision(out m_data.m_KickPrecision);

            // Tackle behaviour.

            aiParams.GetTackleRadius(out m_data.m_TackleRadius);
            aiParams.GetBallDistanceThreshold(out m_data.m_BallDistanceThreshold);

            // Attract behaviour.

            aiParams.GetAttractMinRadius(out m_data.m_AttractMinRadius);
            aiParams.GetAttractMaxRadius(out m_data.m_AttractMaxRadius);

            aiParams.GetAttractTimeThreshold(out m_data.m_AttractTimeThreshold);

            // Extra parameters.

            aiParams.GetRecoverRadius(out m_data.m_RecoverRadius);
            aiParams.GetRecoverTimeThreshold(out m_data.m_RecoverTimeThreshold);

            UpdateSharedInputData(0);
            
            m_behavior_tree.EnableBehavior();
        }

        public override void Setup(tnBaseAIData i_Data)
        {
            base.Setup(i_Data);
            
            //SETUP STATIC GLOBAL DATA
            m_data.ball = ball;
            m_data.ballRadius = ballRadius;            

            m_data.teamCharactersCount = teamCharactersCount;
            m_data.teammatesCount = teammatesCount;
            m_data.opponentsCount = opponentsCount;
            m_data.myGoal = myGoal;
            m_data.opponentGoal = opponentGoal;

            m_data.fieldWidth = fieldWidth;
            m_data.fieldHeight = fieldHeight;
            
            m_data.gkAreaMinHeight = gkAreaMinHeight;
            m_data.gkAreaMaxHeight = gkAreaMaxHeight;
            m_data.gkAreaWidth = gkAreaWidth;
            m_data.gkAreaHeight = gkAreaHeight;

            m_data.goalWidth = goalWidth;
            m_data.goalMaxHeight = goalMaxHeight;
            m_data.goalMinHeight = goalMinHeight;

            m_data.colliderRadius = colliderRadius;
            
            m_data.m_Opponents = opponents;
            m_data.m_Teams = teams;
        }

        public override void Fill(float i_FrameTime, tnInputData i_Data)
        {
            if (!initialized || self == null)
            {
                ResetInputData(i_Data);
                return;
            }

            if (m_Role == AIRole.Null)
            {
                ResetInputData(i_Data);
                return;
            }

            UpdateSharedInputData(i_FrameTime);
            
            // Fill input data.
            if (m_behavior_tree.GetVariable("OutputGameData").GetValue() is OutputGameData output)
            {
                UpdateTimers(output, i_FrameTime);

                var m_Axes = output.axes;
                var requestKick = output.kickButton || output.tackleRequested;
                
                i_Data.SetAxis(InputActions.s_HorizontalAxis, m_Axes.x);
                i_Data.SetAxis(InputActions.s_VerticalAxis, m_Axes.y);
            
                i_Data.SetButton(InputActions.s_PassButton, requestKick);
                i_Data.SetButton(InputActions.s_ShotButton, output.dashButton);
            
                i_Data.SetButton(InputActions.s_TauntButton, output.tauntButton);
                i_Data.SetButton(InputActions.s_AttractButton, output.isAttracting);
            }
            else
            {
                Debug.LogError("No output set!");
            }
        }
        
        public override void Clear()
        {
            m_behavior_tree.SetVariableValue("OutputGameData", new OutputGameData());
            m_behavior_tree.SetVariableValue("GameTimers", new GameTimers());
        }

        private void UpdateSharedInputData(float i_FrameTime)
        {
            m_data.frameTime = i_FrameTime;
            
            m_data.myPosition = myPosition;
            
            m_data.isEnergyEnoughToKick = CheckEnergy(m_data.m_MinKickEnergy);
            m_data.isEnergyEnoughToAttract = CheckEnergy(m_data.m_MinAttractEnergy);
            m_data.isEnergyEnoughToTackle = CheckEnergy(m_data.m_MinTackleEnergy);
            m_data.isEnergyEnoughToDash = CheckEnergy(m_data.m_MinDashEnergy);
            
            m_data.ballDistance = ballDistance;
            m_data.referencePos = referencePosition;
            
            m_data.topLeft = topLeft;
            m_data.topRight = topRight;
            m_data.bottomLeft = bottomLeft;
            m_data.bottomRight = bottomRight;

            m_data.ballPosition = ball != null ? ball.position : Vector3.zero;

            m_data.midfield = midfield;
            
            m_behavior_tree.SetVariableValue("InputGameData", m_data);
        }

        private void UpdateTimers(OutputGameData output, float delta)
        {
            UpdateCooldownTimers(delta);
            UpdateRecoverTimer(delta);
            UpdateAttractTimer(output, delta);
            
            m_behavior_tree.SetVariableValue("GameTimers", m_timers);
        }
        
        private void UpdateAttractTimer(OutputGameData output,float delta)
        {
            if (output.isAttracting)
            {
                m_timers.attractTimer += delta;
            }
            else
            {
                m_timers.attractTimer = 0f;
            }
        }
        
        private void UpdateRecoverTimer(float delta)
        {
            var ballSpeed = CicaUtils.GetSpeed(m_data.ball.gameObject);
            var mySpeed = CicaUtils.GetSpeed(self);

            if (m_data.ballDistance < m_data.m_RecoverRadius && (ballSpeed < 0.15f && mySpeed < 0.15f))
            {
               m_timers.recoverTimer += delta;
            }
            else
            {
                m_timers.recoverTimer = 0f;
            }

        }
        private void UpdateCooldownTimers(float delta)
        {
            if (m_timers.kickCooldownTimer > 0f)
            {
                m_timers.kickCooldownTimer -= delta;

                if (m_timers.kickCooldownTimer < 0f)
                {
                    m_timers.kickCooldownTimer = 0f;
                }
            }

            if (m_timers.dashCooldownTimer > 0f)
            {
                m_timers.dashCooldownTimer -= delta;

                if (m_timers.dashCooldownTimer < 0f)
                {
                    m_timers.dashCooldownTimer = 0f;
                }
            }

            if (m_timers.tackleCooldownTimer > 0f)
            {
                m_timers.tackleCooldownTimer -= delta;

                if (m_timers.tackleCooldownTimer < 0f)
                {
                    m_timers.tackleCooldownTimer = 0f;
                }
            }

            if (!(m_timers.attractCooldownTimer > 0f)) return;
            m_timers.attractCooldownTimer -= delta;

            if (m_timers.attractCooldownTimer < 0f)
            {
                m_timers.attractCooldownTimer = 0f;
            }
        }
    }
}