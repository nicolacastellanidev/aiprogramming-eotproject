﻿using TrueSync;
using UnityEngine;

namespace Nick_Nick
{
    public static class CicaUtils
    {
        public static Vector2 ToVector2(this Vector3 pos)
        {
            return new Vector2(pos.x, pos.y);
        }
        
        public static float GetSpeed(GameObject of)
        {
            if (of is null) return 0f;
            var rigidbody2d = of.GetComponent<TSRigidBody2D>();
            if (rigidbody2d == null) return 0f;
            var tsVelocity = rigidbody2d.velocity;
            var tsSpeed = tsVelocity.magnitude;
            var speed = tsSpeed.AsFloat();
            return speed;
        }
        
        public static Vector2 GetVelocity(GameObject of)
        {
            if (of == null)
            {
                return Vector2.zero;
            }

            var rigidbody2d = of.GetComponent<TSRigidBody2D>();
            if (rigidbody2d == null) return Vector2.zero;
            var tsVelocity = rigidbody2d.velocity;
            Vector2 velocity = tsVelocity.ToVector();
            return velocity;
        }
    }
}