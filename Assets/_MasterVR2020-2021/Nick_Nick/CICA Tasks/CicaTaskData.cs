﻿using System;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks
{

    public class CicaTaskData
    {
        protected Task m_task;
        
        public GameObject self;
        // -------- INPUT
        public InputGameData input;
        // -------- TIMERS
        public GameTimers timers;

        protected Behavior m_owner => m_task.Owner;
        
        public void Init(Task task)
        {
            m_task = task;
        }
        
        public virtual void OnStart()
        {
            self = m_owner.GetVariable("Self").GetValue() as GameObject;
        }

        public virtual bool UpdateData()
        {
            input = (InputGameData) m_owner.GetVariable("InputGameData").GetValue();
            timers = (GameTimers) m_owner.GetVariable("GameTimers").GetValue();
            if (input == null) throw new Exception("InputGameData is null!");
            if (timers == null) throw new Exception("GameTimers is null!");

            return true;
        }

        public bool IsCharacterBehind(Vector2 position, Vector2 target)
        {
            // @todo myGoalPosition should be an enum (LEFT, RIGHT)
            Vector2 myGoalPosition = input.myGoal.transform.position;

            return ((myGoalPosition.x > 0f && position.x > target.x) ||
                    (myGoalPosition.x < 0f && position.x < target.x));
        }

        public bool IsNearTo(Vector2 what, Vector2 to, float offset = 0)
        {
            var position = GetFieldPosition(to);

            if (position.x < input.midfield.x) // Left goal.
            {
                return (what.x < (position.x + offset));
            }
            else // Right goal.
            {
                return (what.x > (position.x - offset));
            }
        }
        
        public bool IsBallComingAgainstMe()
        {
            //is ball coming against me? 
            Vector2 ballVelocity = CicaUtils.GetVelocity(input.ball.gameObject).normalized;
            Vector2 ballToMeDir = (input.myPosition - input.ballPosition.ToVector2()).normalized;
            float AreDirectionsSimilar = Vector2.Dot(ballToMeDir, ballVelocity);
            return AreDirectionsSimilar > 0.65;
        }

        public bool IsBallGoingToReachAnAlly(float reachingTolerance = 0.65f)
        {
            float tol = Mathf.Clamp01(reachingTolerance);
            
            Vector2 ballVelocity = CicaUtils.GetVelocity(input.ball.gameObject);
            float ballDisplacement = (ballVelocity * input.frameTime).magnitude;
            
            foreach (var ally in input.m_Teams)
            {
                Vector2 ballToAlly = (ally.position - input.ballPosition).normalized;
                //verify ball velocity and ball to ally direction are similar
                float AreDirectionsSimilar = Vector2.Dot(ballToAlly, ballVelocity.normalized);
                
                if (!(AreDirectionsSimilar > tol)) continue;
                
                float ballToAllyDistance = (ally.position - input.ballPosition).magnitude;

                float allyDisplacementTowardsBall = 0; //displacement of the selected ally towards ball in this frame
                Vector2 allyVelocity = CicaUtils.GetVelocity(ally.gameObject);
                //if ally is moving (even very slowly) and its moving towards the ball
                if (allyVelocity.magnitude >= 0.1f && Vector2.Dot(allyVelocity, ballToAlly) <= -tol)
                {
                    allyDisplacementTowardsBall = (allyVelocity * input.frameTime).magnitude; // store displacement towards the ball
                }
                //sum ally displacement towards ball and ball displacement towards ally to verify if the distance between the two is covered
                return (allyDisplacementTowardsBall + ballDisplacement) >= ballToAllyDistance;
            }
            
            return false;
        }

        public bool IsNearestToMyGoal()
        {
            Transform nearest = null;
            var minDistance = float.MaxValue;

            for (var teammateIndex = 0; teammateIndex < input.teamCharactersCount; ++teammateIndex)
            {
                var teammate = input.m_Teams[teammateIndex];
                if (teammate == null) continue;
                var teammatePosition = teammate.position.ToVector2();
                var toTarget = input.myGoal.position.ToVector2() - teammatePosition;
                var distance = toTarget.magnitude;
                if (!(distance < minDistance)) continue;
                nearest = teammate;
                minDistance = distance;
            }

            return nearest == self.transform;
        }
        
        public bool IsThereAnOpponentNearbyTheBall(float radius)
        {
            foreach (var opponent in input.m_Opponents)
            {
                if (opponent == null) continue;

                Vector2 opponentPosition = opponent.position;
                var distance = Vector2.Distance(input.ballPosition, opponentPosition);

                if (distance <= radius) return true;
            }

            return false;
        }
        
        public Vector2 GetFieldPosition(Vector2 position)
        {
            // Check left.
            var halfFieldWidth = input.fieldWidth / 2.0f;
            var halfFieldHeight = input.fieldHeight / 2.0f;

            var leftThreshold = input.midfield.x - halfFieldWidth;

            if (position.x < leftThreshold)
            {
                position.x = leftThreshold;
            }

            // Check right.
            var rightThreshold = input.midfield.x + halfFieldWidth;

            if (position.x > rightThreshold)
            {
                position.x = rightThreshold;
            }

            // Check top.

            var topThreshold = input.midfield.y + halfFieldHeight;

            if (position.y > topThreshold)
            {
                position.y = topThreshold;
            }

            // Check bottom.
            var bottomThreshold = input.midfield.y - halfFieldHeight;

            if (position.y < bottomThreshold)
            {
                position.y = bottomThreshold;
            }

            return position;
        }

        public bool IsThereAMateInMyGKArea()
        {
            foreach (var mate in input.m_Teams)
            {
                Vector2 matePos = mate.position;
                Vector2 myGoalReference = GetFieldPosition(input.myGoal.position);

                if (myGoalReference.x < input.midfield.x) // Left goal.
                {
                    return (matePos.x < (myGoalReference.x + input.gkAreaWidth)) && (matePos.y > input.gkAreaMinHeight && matePos.y < input.gkAreaMaxHeight);
                }
                else
                {
                    return (matePos.x > (myGoalReference.x - input.gkAreaWidth)) &&
                           (matePos.y > input.gkAreaMinHeight && matePos.y < input.gkAreaMaxHeight);
                }
            }

            return false;
        }
        
        public void ComputeFieldPosition(out Vector2 o_Position)
        {
            var target = Vector2.zero;

            // Compute sepration.

            {
                Vector2 separationTarget;
                ComputeSeparationTarget(out separationTarget);

                target += separationTarget;
            }

            // Seek/Flee ball.

            {
                var fleeMinDistance = input.fieldHeight * input.m_MinFleeDistanceFactor;
                if (input.ballDistance < fleeMinDistance)
                {
                    var seekTarget = input.myPosition;
                    ComputeFleeTarget(input.ballPosition, fleeMinDistance, out seekTarget);

                    target += seekTarget;
                    target *= 0.5f;
                }
                else
                {
                    var fleeMaxDistance = input.fieldHeight * input.m_MaxFleeDistanceFactor;
                    if (input.ballDistance > fleeMaxDistance)
                    {
                        var seekTarget = input.myPosition;
                        ComputeSeekTarget(input.ballPosition.ToVector2(), fleeMaxDistance, out seekTarget);

                        target += seekTarget;
                        target *= 0.5f;
                    }
                }
            }

            o_Position = target;
        }

        private void ComputeSeparationTarget(out Vector2 o_Target)
        {
            var sum = Vector2.zero;
            var count = 0;

            for (var teammateIndex = 0; teammateIndex < input.teammatesCount; ++teammateIndex)
            {
                var teammate = GetTeammateByIndex(teammateIndex);
                if (teammate == null) continue;
                var teammatePosition = teammate.position.ToVector2();
                var teammateToSelf = input.myPosition - teammatePosition;

                var distance = teammateToSelf.magnitude;

                if (!(distance < input.m_SeparationThreshold)) continue;
                var teammateToSelfDirection = teammateToSelf.normalized;

                var target = teammatePosition + teammateToSelfDirection * input.m_SeparationThreshold;
                sum += target;

                ++count;
            }

            if (count > 0)
            {
                var separationTarget = sum / count;
                o_Target = separationTarget;
            }
            else
            {
                o_Target = input.myPosition;
            }
        }

        private void ComputeSeekTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
        {
            var selfToSource = i_Source - input.myPosition;
            var distanceToSource = selfToSource.magnitude;
            var selfToSourceDirection = selfToSource.normalized;
            o_Target = input.myPosition + selfToSourceDirection * (distanceToSource - i_Threshold);
        }

        private Transform GetTeammateByIndex(int i_Index)
        {
            if (i_Index < 0 || i_Index >= input.m_Teams.Count)
            {
                return null;
            }

            return input.m_Teams[i_Index];
        }

        private void ComputeFleeTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
        {
            var sourceToSelfDirection = input.myPosition - i_Source;
            sourceToSelfDirection.Normalize();
            o_Target = i_Source + sourceToSelfDirection * i_Threshold;
        }
        
        public Vector2 GetBallDirection(Vector2 i_From)
        {
            Vector2 ballPositionVec2 = input.ballPosition;
            Vector2 toBall = ballPositionVec2 - i_From;
            Vector2 toBallDirection = toBall.normalized;

            return toBallDirection;
        }

        public Transform GetOpponentNearestTo(Vector3 position, List<Transform> opponents)
        {
            Transform nearest = null;
            var minDistance = float.MaxValue;

            foreach (var opponent in opponents)
            {
                if (opponent == null) continue;

                Vector2 opponentPosition = opponent.position;
                var toTarget = (Vector2) position - opponentPosition;
                var distance = toTarget.magnitude;

                if (!(distance <= minDistance)) continue;
                nearest = opponent;
                minDistance = distance;
            }

            return nearest;
        }

        public Vector2 Seek(Vector2 target, float tolerance = 0f)
        {
            if (self == null)
            {
                throw new NullReferenceException("Self is not defined");
            }

            var difference = target - input.myPosition;

            return difference.sqrMagnitude < tolerance * tolerance ? Vector2.zero : difference.normalized;
        }

        public Vector2 ClampPosition(Vector2 position, float tolerance = 0f)
        {
            // Check left.
            var halfFieldWidth = input.fieldWidth / 2.0f;
            var halfFieldHeight = input.fieldHeight / 2.0f;

            var leftThreshold = input.midfield.x - halfFieldWidth;
            leftThreshold += tolerance;

            if (position.x < leftThreshold)
            {
                position.x = leftThreshold;
            }

            // Check right.

            var rightThreshold = input.midfield.x + halfFieldWidth;
            rightThreshold -= tolerance;

            if (position.x > rightThreshold)
            {
                position.x = rightThreshold;
            }

            // Check top.

            var topThreshold = input.midfield.y + halfFieldHeight;
            topThreshold -= tolerance;

            if (position.y > topThreshold)
            {
                position.y = topThreshold;
            }

            // Check bottom.
            var bottomThreshold = input.midfield.y - halfFieldHeight;
            bottomThreshold += tolerance;

            if (position.y < bottomThreshold)
            {
                position.y = bottomThreshold;
            }

            return position;
        }
        
        public Vector2 GetMyGoalDirection(Vector2 from)
        {
            if (input.myGoal == null)
            {
                return Vector2.zero;
            }

            var toMyGoal = input.myGoal.position.ToVector2() - from;
            var toMyGoalDirection = toMyGoal.normalized;

            return toMyGoalDirection;
        }

        public void ComputeGkPosition(out Vector2 position, float ballSpeedMagnitude = 0.1f)
        {
            var gkPosition = GetFieldPosition(input.myGoal.position.ToVector2());

            var idealHeight = input.ballPosition.y;
            
            var ballVelocity = CicaUtils.GetVelocity(input.ball.gameObject);

            if (ballVelocity.magnitude > ballSpeedMagnitude)
            {
                var ballDirection = ballVelocity.normalized;
                var ballToGoalDirection = GetMyGoalDirection(input.ball.position.ToVector2());

                var dot = Vector2.Dot(ballDirection, ballToGoalDirection);

                if (dot > 0f)
                {
                    var position1 = input.ball.position;
                    idealHeight = position1.y + (gkPosition.x - position1.x) * (ballVelocity.y / ballVelocity.x);
                }
            }

            var goalMinHeight = gkPosition.y - input.goalWidth / 2f;
            var goalMaxHeight = gkPosition.y + input.goalWidth / 2f;

            
            // removes my size from boundaries
            //goalMinHeight += input.colliderRadius;
            //goalMaxHeight -= input.colliderRadius;
            
            idealHeight = Mathf.Clamp(idealHeight, goalMinHeight, goalMaxHeight);

            if (gkPosition.x > input.midfield.x) // Right goal.
            {
                gkPosition.x -= input.colliderRadius;
                gkPosition.x -= 0.15f;
            }
            else // Left goal.
            {
                gkPosition.x += input.colliderRadius;
                gkPosition.x += 0.15f;
            }

            gkPosition.y = idealHeight;
            position = gkPosition;
        }
        
        public void GetDistanceAvoidingBall(Vector2 position, out Vector2 targetDistance, out Vector2 targetDirection)
        {
            targetDistance = position - input.myPosition;
            targetDirection = targetDistance.normalized;

            {
                // Apply direction correction in order to avoid the ball.

                Vector2 pointA = input.ballPosition;
                pointA.y += input.ballRadius;
                pointA.y += input.colliderRadius;
                pointA.y += 0.05f; // Safety tolerance.

                Vector2 pointB = input.ballPosition;
                pointB.y -= input.ballRadius;
                pointB.y -= input.colliderRadius;
                pointB.y -= 0.05f; // Safety tolerance.

                var selfToA = pointA - input.myPosition;
                var selfToB = pointB - input.myPosition;

                var directionA = selfToA.normalized;
                var directionB = selfToB.normalized;

                var angleA = Vector2.Angle(targetDirection, directionA);
                var angleB = Vector2.Angle(targetDirection, directionB);

                var angleAB = Vector2.Angle(directionA, directionB);

                if (angleA < angleAB && angleB < angleAB)
                {
                    // Target direction is into ball fov. Apply a correction using the shortest line.
                    targetDirection = selfToA.sqrMagnitude < selfToB.sqrMagnitude ? directionA : directionB;
                }
                else
                {
                    // Target direction is fine. Nothing to do.
                }
            }
        }
    }
}