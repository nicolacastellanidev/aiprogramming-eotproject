using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsMyGoalPositionInFieldSide : CicaBaseConditional
	{
		public SharedFieldSide fieldSide;
		protected override TaskStatus Execute()
		{
			return CheckPositionInsideSelectedField(fieldSide.Value) ? TaskStatus.Success : TaskStatus.Failure;
		}

		private bool CheckPositionInsideSelectedField(FieldSide type)
		{
			if (type == FieldSide.Left)
				return m_taskData.input.myGoal.position.x < m_taskData.input.midfield.x;
			else
				return m_taskData.input.myGoal.position.x > m_taskData.input.midfield.x;
		}
	}
}