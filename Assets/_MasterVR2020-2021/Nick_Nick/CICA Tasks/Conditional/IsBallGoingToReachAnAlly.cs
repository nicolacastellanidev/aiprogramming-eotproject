using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsBallGoingToReachAnAlly : CicaBaseConditional
    {
        protected override TaskStatus Execute()
        {
            return m_taskData.IsBallGoingToReachAnAlly() ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}