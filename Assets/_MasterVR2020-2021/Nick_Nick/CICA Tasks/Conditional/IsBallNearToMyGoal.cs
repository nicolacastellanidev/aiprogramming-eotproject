using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsBallNearToMyGoal : CicaBaseConditional
    {
        protected override TaskStatus Execute()
        {
            return m_taskData.IsNearTo(m_taskData.input.ballPosition, m_taskData.input.myGoal.position.ToVector2(), m_taskData.input.gkAreaWidth) ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}