using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class CanAttract : CicaBaseConditional
	{
		protected override TaskStatus Execute()
		{
			if (!m_taskData.input.isEnergyEnoughToAttract) 
				return TaskStatus.Failure;
			if (m_taskData.timers.attractCooldownTimer != 0f || (m_taskData.timers.attractTimer >= m_taskData.input.m_AttractTimeThreshold))
				return TaskStatus.Failure;
			if (m_taskData.input.ballDistance >= m_taskData.input.m_AttractMaxRadius) 
				return TaskStatus.Failure;
			
			return TaskStatus.Success;
		}
	}
}