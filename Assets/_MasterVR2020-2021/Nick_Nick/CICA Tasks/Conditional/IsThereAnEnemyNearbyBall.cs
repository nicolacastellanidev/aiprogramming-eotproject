using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsThereAnEnemyNearbyBall : CicaBaseConditional
	{
		public SharedFloat BonusRange = 0.5f;
		protected override TaskStatus Execute()
		{
			return m_taskData.IsThereAnOpponentNearbyTheBall(BonusRange.Value) 
				? TaskStatus.Success 
				: TaskStatus.Failure;
		}
	}
}