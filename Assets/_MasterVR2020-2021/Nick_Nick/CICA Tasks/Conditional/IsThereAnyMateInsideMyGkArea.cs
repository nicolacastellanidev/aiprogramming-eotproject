using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsThereAnyMateInsideMyGkArea : CicaBaseConditional
	{
		protected override TaskStatus Execute()
		{
			return m_taskData.IsThereAMateInMyGKArea() ? TaskStatus.Success : TaskStatus.Failure;
		}
	}
}