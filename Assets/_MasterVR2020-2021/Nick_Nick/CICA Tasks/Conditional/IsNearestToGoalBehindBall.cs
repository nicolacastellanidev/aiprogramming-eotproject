using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsNearestToGoalBehindBall : CicaBaseConditional
    {

        protected override TaskStatus Execute()
        {
            Transform nearest = null;
            var minDistance = float.MaxValue;

            for (var teammateIndex = 0; teammateIndex < m_taskData.input.teamCharactersCount; ++teammateIndex)
            {
                var teammate = m_taskData.input.m_Teams[teammateIndex];
                if (teammate == null) continue;
                var teammatePosition = teammate.position.ToVector2();
                if (!m_taskData.IsCharacterBehind(teammatePosition, m_taskData.input.ballPosition.ToVector2())) continue;
                var toTarget = m_taskData.input.myGoal.position.ToVector2() - teammatePosition;
                var distance = toTarget.magnitude;
                if (!(distance < minDistance)) continue;
                nearest = teammate;
                minDistance = distance;
            }

            return nearest == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}