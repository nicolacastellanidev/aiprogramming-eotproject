using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class ShouldIPassMidfield : CicaBaseConditional
    {
        public SharedInt AllowedTeammatesToEvaluateCondition = 2;

        protected override TaskStatus Execute()
        {
            if (m_taskData.input.teammatesCount < AllowedTeammatesToEvaluateCondition.Value) return TaskStatus.Success;

            return m_taskData.IsNearestToMyGoal()
                ? TaskStatus.Failure
                : TaskStatus.Success;
        }
    }
}