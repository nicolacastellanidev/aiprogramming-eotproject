using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsAbleToScore : CicaBaseConditional
    {
        protected override TaskStatus Execute()
        {
            Vector2 opponentGoalReference = m_taskData.GetFieldPosition(m_taskData.input.opponentGoal.position);

            bool IsNearbyOpponentGoal = false;
            if (opponentGoalReference.x < m_taskData.input.midfield.x) // Left goal.
            {
                IsNearbyOpponentGoal =  (m_taskData.input.myPosition.x < (opponentGoalReference.x + m_taskData.input.gkAreaWidth));
            }
            else // Right goal.
            {
                IsNearbyOpponentGoal = (m_taskData.input.myPosition.x > (opponentGoalReference.x - m_taskData.input.gkAreaWidth));
            }
            
            return IsNearbyOpponentGoal
                ? TaskStatus.Success
                : TaskStatus.Failure;
        }
    }
}