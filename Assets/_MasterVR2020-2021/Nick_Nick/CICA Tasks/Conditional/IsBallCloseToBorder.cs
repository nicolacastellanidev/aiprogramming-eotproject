using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsBallCloseToBorder : CicaBaseConditional
	{
		public SharedBorder border;
		public SharedFloat m_tolerance = 0.1f;

		protected override TaskStatus Execute()
		{
			var tolerance = m_taskData.input.colliderRadius;
			tolerance += m_tolerance.Value;

			return NearBorder(tolerance) ? TaskStatus.Success : TaskStatus.Failure;
		}

		private bool NearBorder(float tolerance)
		{
			switch (border.Value)
			{
				case Border.Top:
					return (m_taskData.input.topLeft.y - m_taskData.input.ballPosition.y < tolerance);
				case Border.Left:
					return (m_taskData.input.ballPosition.x - m_taskData.input.topLeft.x < tolerance);
				case Border.Bottom:
					return (m_taskData.input.ballPosition.y - m_taskData.input.bottomRight.y < tolerance);
				case Border.Right:
					return (m_taskData.input.bottomRight.x - m_taskData.input.ballPosition.x < tolerance);
				default:
					return false;
			}
		}
	}
}