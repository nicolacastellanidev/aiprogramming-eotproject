using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public abstract class CicaBaseConditional : BehaviorDesigner.Runtime.Tasks.Conditional
    {
        protected CicaTaskData m_taskData = new CicaTaskData(); // custom implementation
        
        /**
         * Called on conditional awake, sets the task data
         */
        public override void OnAwake()
        {
            base.OnAwake();
            m_taskData.Init(this);
        }

        /**
         * Called on start, call start on task data
         */
        public override void OnStart()
        {
            base.OnStart();
            m_taskData.OnStart();
        }

        /**
         * Called on update, tries to update data and execute next
         */
        public override TaskStatus OnUpdate()
        {
            if (!m_taskData.UpdateData()) return TaskStatus.Failure;
            return Execute();
        }

        /**
         * Custom implementation
         */
        protected abstract TaskStatus Execute();
    }
}