using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsBallCloseToAnyBorder : CicaBaseConditional
    {
        public SharedFloat m_tolerance = 0.1f;
        protected override TaskStatus Execute()
        {
            bool IsCloseToAnyBorder = false;

            var tolerance = m_taskData.input.colliderRadius;
            tolerance += m_tolerance.Value;

            IsCloseToAnyBorder = (m_taskData.input.topLeft.y - m_taskData.input.ballPosition.y < tolerance); //TOP 
            IsCloseToAnyBorder = IsCloseToAnyBorder || (m_taskData.input.ballPosition.x - m_taskData.input.topLeft.x < tolerance); //LEFT
            IsCloseToAnyBorder = IsCloseToAnyBorder || (m_taskData.input.ballPosition.y - m_taskData.input.bottomRight.y < tolerance); //BOTTOM
            IsCloseToAnyBorder = IsCloseToAnyBorder || (m_taskData.input.bottomRight.x - m_taskData.input.ballPosition.x < tolerance); //RIGHT
            
            return IsCloseToAnyBorder
                ? TaskStatus.Success
                : TaskStatus.Failure;
        }
    }
}