using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsAttractionDirectionEnforced : CicaBaseConditional
	{
		public SharedBool IsAttractionDirectionBeenEnforced;
		
		protected override TaskStatus Execute()
		{
			return IsAttractionDirectionBeenEnforced.Value ? TaskStatus.Success : TaskStatus.Failure;
		}
	}
}