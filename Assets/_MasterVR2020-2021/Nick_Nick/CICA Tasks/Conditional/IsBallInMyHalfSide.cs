using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsBallInMyHalfSide : CicaBaseConditional
    {

        protected override TaskStatus Execute()
        {
            var positionX = m_taskData.input.ballPosition.x;
            var myGoalX = m_taskData.input.myGoal.transform.position.x;
            var midfieldX = m_taskData.input.midfield.x;
            if ((myGoalX > midfieldX && positionX > midfieldX) || (myGoalX < midfieldX && positionX < midfieldX))
            {
                return TaskStatus.Success;
            }

            return TaskStatus.Failure;
        }
    }
}