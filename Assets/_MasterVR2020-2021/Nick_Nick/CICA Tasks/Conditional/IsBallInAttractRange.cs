using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsBallInAttractRange : CicaBaseConditional
	{
		protected override TaskStatus Execute()
		{
			bool IsBallInRange = m_taskData.input.ballDistance < m_taskData.input.m_AttractMinRadius;
			return IsBallInRange ? TaskStatus.Success : TaskStatus.Failure;
		}
	}
}