using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsNearestToGoal : CicaBaseConditional
    {

        protected override TaskStatus Execute()
        {
            return m_taskData.IsNearestToMyGoal()
                ? TaskStatus.Success
                : TaskStatus.Failure;
        }
    }
}