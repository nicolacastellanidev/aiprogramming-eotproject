using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsNearestToMateChargingBall : CicaBaseConditional
    {
        public SharedInt IsNotAllowedWithTeammatesCountLessThan = 2;
        public IsNearestToBall refNearestToBall;
        protected override TaskStatus Execute()
        {
            if (m_taskData.input.teammatesCount < IsNotAllowedWithTeammatesCountLessThan.Value)
                return TaskStatus.Failure;
            
            Transform teammateNearestBall = refNearestToBall.nearest.Value;
            Transform nearest = null;
            float minDistance = float.MaxValue;

            foreach (var teammate in m_taskData.input.m_Teams)
            {
                if (teammate == null || teammateNearestBall == teammate) continue;
                
                Vector2 teammatePosition = teammate.position;
                Vector2 toTarget = teammateNearestBall.position.ToVector2() - teammatePosition;
                float distance = toTarget.magnitude;
                if (distance <= minDistance)
                {
                    nearest = teammate;
                    minDistance = distance;
                }
            }

            return nearest == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}