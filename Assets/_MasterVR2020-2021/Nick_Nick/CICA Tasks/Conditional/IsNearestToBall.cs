using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsNearestToBall : CicaBaseConditional
    {
        public SharedTransform nearest;
        protected override TaskStatus Execute()
        {
            nearest.Value = null;
            var minDistance = float.MaxValue;

            foreach (var teammate in m_taskData.input.m_Teams)
            {
                if (teammate == null) continue;
                Vector2 teammatePosition = teammate.position;
                var toTarget = m_taskData.input.ballPosition.ToVector2() - teammatePosition;
                var distance = toTarget.magnitude;
                if (!(distance < minDistance)) continue;
                nearest.Value = teammate;
                minDistance = distance;
            }
            
            return nearest.Value == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}