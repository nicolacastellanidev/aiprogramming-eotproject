using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Conditional
{
    public class IsBehindBall : CicaBaseConditional
    {
        protected override TaskStatus Execute()
        {
            return m_taskData.IsCharacterBehind(m_taskData.input.myPosition, m_taskData.input.ballPosition) ? TaskStatus.Success : TaskStatus.Failure;
        }
    }
}