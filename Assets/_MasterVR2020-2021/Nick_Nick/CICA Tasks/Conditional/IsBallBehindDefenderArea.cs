using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Conditional
{
	public class IsBallBehindDefenderArea : CicaBaseConditional
	{
		public SharedFloat Weight = 0.5f;
		protected override TaskStatus Execute()
		{
			Vector2 myGoalPos = m_taskData.input.myGoal.position.ToVector2();
			float w = Mathf.Clamp01(Weight.Value);
			return m_taskData.IsNearTo(
				m_taskData.input.ballPosition,
				myGoalPos,
				Mathf.Abs(myGoalPos.x - m_taskData.input.midfield.x) * w
				) 
				? TaskStatus.Success 
				: TaskStatus.Failure;

		}
	}
}