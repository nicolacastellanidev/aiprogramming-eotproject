using System;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;
using UnityEngine.UI;

namespace Nick_Nick.CICA_Tasks
{
    public class CicaActionData : CicaTaskData
    {
        public OutputGameData output = new OutputGameData();

        // -------- GAMEOBJECTS
        public Text debugText;
        
        // -------- SETTINGS
        public bool log = true;

        public override void OnStart()
        {
            base.OnStart();
            var debugTextGO = m_owner.GetVariable("DebugText").GetValue() as GameObject;
            if (debugTextGO && !Application.isEditor)
            {
                debugTextGO.SetActive(false);
            }
            else if(debugTextGO)
            {
                debugText = debugTextGO.GetComponent<Text>();
            }
        }

        public override bool UpdateData()
        {
            base.UpdateData();
            output = (OutputGameData) m_owner.GetVariable("OutputGameData").GetValue();
            if (output == null) throw new Exception("OutputGameData is null!");

            return true;
        }
    }
}