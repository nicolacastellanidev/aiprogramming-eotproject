using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
	public class AttractBallInDirection : CicaBaseAction
	{
		public SharedBool forceAttractDirection;
		public SharedBool ShouldResetAxes;
		public SharedVector2 attractDirection;
		protected override TaskStatus Execute()
		{
			Vector2 axes = ShouldResetAxes.Value
				? attractDirection.Value
				: m_actionData.output.axes + attractDirection.Value;
			m_actionData.output = OutputGameDataBuilder.Builder()
				.WithAxes(axes)
				.WithAttract(true)
				.Build();
			forceAttractDirection.SetValue(true);
			return TaskStatus.Success;
		}
	}
}