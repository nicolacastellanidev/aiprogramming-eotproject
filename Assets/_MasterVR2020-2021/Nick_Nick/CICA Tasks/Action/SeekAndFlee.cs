using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class SeekAndFlee : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
            m_actionData.ComputeFieldPosition(out var target);
            m_actionData.output =
                new OutputGameDataBuilder()
                    .WithAxes(m_actionData.Seek(target, m_actionData.input.colliderRadius))
                    .Build();
            return TaskStatus.Running;
        }
    }
}