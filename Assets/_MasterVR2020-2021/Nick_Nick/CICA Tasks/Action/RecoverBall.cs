using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class RecoverBall : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
            m_actionData.GetDistanceAvoidingBall(m_actionData.input.ballPosition.ToVector2(), out var targetDistance, out var targetDirection);

            var axes = targetDirection.normalized;
            var dashButton = !m_actionData.IsBallComingAgainstMe() && targetDistance.sqrMagnitude > (m_actionData.input.m_ForcedDashDistance * m_actionData.input.m_ForcedDashDistance);

            m_actionData.output = 
                new OutputGameDataBuilder()
                    .WithAxes(axes)
                    .WithDash(dashButton)
                    .Build();
            
            
            return TaskStatus.Running;
        }
    }
}