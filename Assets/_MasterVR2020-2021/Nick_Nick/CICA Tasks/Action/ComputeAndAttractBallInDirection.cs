using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
	public class ComputeAndAttractBallInDirection : CicaBaseAction
	{
		protected override TaskStatus Execute()
		{
			var selfPosition = m_actionData.input.myPosition;
			
			Vector2 myGoalDirection = m_actionData.GetMyGoalDirection(selfPosition);
			Vector2 ballDirection = m_actionData.GetBallDirection(selfPosition);
			
			Vector2 axisA = new Vector2(ballDirection.y, -ballDirection.x);
			Vector2 axisB = -axisA;

			float dotA = Vector2.Dot(axisA, myGoalDirection);
			float dotB = Vector2.Dot(axisB, myGoalDirection);

			m_actionData.output =
				new OutputGameDataBuilder()
					.WithAxes(dotA > dotB ? axisA : axisB)
					.WithAttract(true)
					.Build();
			
			return TaskStatus.Success;
		}
	}
}