using System;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class CoverGoal : CicaBaseAction
    {
        public SharedFloat Weight = 0.8f;
        protected override TaskStatus Execute()
        {
            float weight = Mathf.Clamp01(Weight.Value);
            weight = 1f - weight;
            
            var ballPosition = m_actionData.input.ballPosition.ToVector2();
            var difference = Vector2.zero;

            difference.y = ballPosition.y - m_actionData.input.myPosition.y;
            float limiterX = (m_actionData.input.fieldWidth / 2f) * weight;
            float maxX = m_actionData.input.myPosition.x > m_actionData.input.midfield.x
                ? m_actionData.input.midfield.x + limiterX
                : m_actionData.input.midfield.x - limiterX;
            difference.x = maxX - m_actionData.input.myPosition.x;
            var goalCenter = m_actionData.input.myGoal.position.ToVector2().y;
            
            // clamp position to goal
            difference.y = Math.Max(goalCenter - (m_actionData.input.goalWidth / 2f), difference.y);
            difference.y = Math.Min(goalCenter + (m_actionData.input.goalWidth / 2f), difference.y);

            m_actionData.output = 
                new OutputGameDataBuilder()
                    .WithAxes(difference.normalized)
                    .Build();
            
            return TaskStatus.Running;
        }
    }
}