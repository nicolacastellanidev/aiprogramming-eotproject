using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class ChargeBall : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
            var axes = Vector2.zero;
            var dashButton = false;
            var kickButton = false;
            
            var target = m_actionData.input.ballPosition.ToVector2();
            var direction = target - m_actionData.input.opponentGoal.position.ToVector2();

            direction.Normalize();

            var offset = m_actionData.input.ballRadius;
            offset += m_actionData.input.colliderRadius;
            offset += 0.05f; // Tolerance threshold.

            target += direction * offset;
            target = m_actionData.ClampPosition(target, 0.2f);
            axes = m_actionData.Seek(target);
            
            var targetDistance = Vector2.Distance(target, m_actionData.input.myPosition);

            if (m_actionData.timers.recoverTimer > m_actionData.input.m_RecoverTimeThreshold)
            {
                kickButton = true; // Force kick to recover.
            }
            else {
                if (targetDistance > m_actionData.input.m_DashDistance)
                {
                    dashButton = !m_actionData.IsBallComingAgainstMe();
                }
                else if (targetDistance <= m_actionData.input.m_KickPrecision)
                {
                    kickButton = true;
                }
            }

            m_actionData.output =
                new OutputGameDataBuilder()
                    .WithAxes(axes)
                    .WithDash(dashButton)
                    .WithKick(kickButton)
                    .Build();
            
            return TaskStatus.Running;
        }
    }
}