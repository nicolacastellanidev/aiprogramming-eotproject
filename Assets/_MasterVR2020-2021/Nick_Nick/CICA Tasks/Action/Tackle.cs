using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class Tackle : CicaBaseAction
    {
        public override void OnAwake()
        {
            base.OnAwake();
            m_actionData.log = false;
        }

        protected override TaskStatus Execute()
        {
            if (m_actionData.output.kickButton
                || m_actionData.input.m_Opponents == null) return TaskStatus.Failure;

            // get the nearest opponent
            var nearestOpponent = m_actionData.GetOpponentNearestTo(m_actionData.input.myPosition, m_actionData.input.m_Opponents);

            // check the distance

            Vector2 nearestOpponentPos = nearestOpponent.position;
            var distance = Vector2.Distance(m_actionData.input.myPosition, nearestOpponentPos);
            
            var tackleRequested = distance < m_actionData.input.m_TackleRadius &&
                                                  m_actionData.input.ballDistance > m_actionData.input.m_BallDistanceThreshold;

            m_actionData.output =
                new OutputGameDataBuilder()
                    .WithTackle(tackleRequested)
                    .Build();
                                                                       
            return TaskStatus.Success;
        }
    }
}