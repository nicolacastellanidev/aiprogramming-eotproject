using System;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public abstract class CicaBaseAction : BehaviorDesigner.Runtime.Tasks.Action
    {
        // -------- OUTPUT
        private SharedOutputGameData shared_Output;

        protected CicaActionData m_actionData = new CicaActionData();

        /**
         * Initializes action data
         */
        public override void OnAwake()
        {
            base.OnAwake();
            m_actionData.Init(this);
        }

        /**
         * Called on game start
         */
        public override void OnStart()
        {
            base.OnStart();
            shared_Output = (SharedOutputGameData) Owner.GetVariable("OutputGameData");
            m_actionData.OnStart();
        }

        /**
         * Executes action implementation:
         *      1. Updates data
         *      2. Shows log name
         *      3. Patches new data after execution
         */
        public override TaskStatus OnUpdate()
        {
            if (!m_actionData.UpdateData()) return TaskStatus.Failure;

            TaskStatus result;
            try
            {
                result = Execute();
                if (m_actionData.log && m_actionData.debugText) m_actionData.debugText.text = FriendlyName;
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return TaskStatus.Failure;
            }

            if (m_actionData.output.kickButton)
                RegularizeKick();
            if (m_actionData.output.dashButton)
                RegularizeDash();
            if (m_actionData.output.tackleRequested)
                RegularizeTackle();
            if(!m_actionData.output.isAttracting)
                RegularizeAttract();
            
            PatchData();
            return result;
        }

        /**
         * Custom implementation
         */
        protected abstract TaskStatus Execute();

        /**
         * Patches the data, which will be handled next from other actions and input fillers
         */
        private void PatchData()
        {
            shared_Output.SetValue(m_actionData.output);
        }

        private void RegularizeAttract()
        {
            if (!m_actionData.output.isAttracting && m_actionData.timers.attractTimer > 0f)
            {
                m_actionData.timers.attractCooldownTimer = m_actionData.input.m_AttractCooldown;
            }
        }
        
        private void RegularizeTackle()
        {
            if (!m_actionData.input.isEnergyEnoughToTackle
                || m_actionData.timers.tackleCooldownTimer > 0f)
            {
                // cannot perform action
                m_actionData.output.tackleRequested = false;
            }
            else
            {
                m_actionData.timers.tackleCooldownTimer = m_actionData.input.m_TackleCooldown;
            }
        }

        private void RegularizeKick()
        {
            if (!m_actionData.input.isEnergyEnoughToKick
                || m_actionData.timers.kickCooldownTimer > 0f)
            {
                // cannot perform action
                m_actionData.output.kickButton = false;
            }
            else
            {
                m_actionData.timers.kickCooldownTimer = m_actionData.input.m_KickCooldown;
            }
        }

        private void RegularizeDash()
        {
            if (!m_actionData.input.isEnergyEnoughToDash
                || m_actionData.timers.dashCooldownTimer > 0f)
            {
                // cannot perform action
                m_actionData.output.dashButton = false;
            }
            else
            {
                m_actionData.timers.dashCooldownTimer = m_actionData.input.m_DashCooldown;
            }
        }
    }
}