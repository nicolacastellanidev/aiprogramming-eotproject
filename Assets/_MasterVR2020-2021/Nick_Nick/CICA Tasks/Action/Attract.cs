using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace Nick_Nick.CICA_Tasks.Action
{
	public class Attract : CicaBaseAction
	{
		public SharedBool IsDirectionEnforced;
		protected override TaskStatus Execute()
		{
			IsDirectionEnforced.Value = false;
			
			return m_actionData.output.isAttracting ? TaskStatus.Running : TaskStatus.Failure;
		}
	}
}