using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick;
using Nick_Nick.CICA_SharedVariables;
using Nick_Nick.CICA_Tasks.Action;

public class RecoverAnAdvancedPosition : CicaBaseAction
{
    protected override TaskStatus Execute()
    {
        m_actionData.GetDistanceAvoidingBall(m_actionData.input.opponentGoal.position.ToVector2(), out var targetDistance, out var targetDirection);
        var axes = targetDirection.normalized;
        var dashButton = targetDistance.sqrMagnitude > m_actionData.input.m_ForcedDashDistance * m_actionData.input.m_ForcedDashDistance;

        m_actionData.output =
            new OutputGameDataBuilder()
                .WithAxes(axes)
                .WithDash(dashButton)
                .Build();
        
        return TaskStatus.Running;
    }
}