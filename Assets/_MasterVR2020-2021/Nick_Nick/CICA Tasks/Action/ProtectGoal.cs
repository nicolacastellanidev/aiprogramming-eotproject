using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class ProtectGoal : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
            // Protect your goal!
            m_actionData.ComputeGkPosition(out var gkPosition, 10f);
            var axes = m_actionData.Seek(gkPosition, m_actionData.input.colliderRadius);
            var kickButton = false;
            // If you can kick the ball away, try to do it.
            var kickRadius = m_actionData.input.colliderRadius;
            kickRadius += m_actionData.input.ballRadius;
            kickRadius += 0.25f; // Safety threshold.

            if (m_actionData.input.ballDistance < kickRadius)
            {
               kickButton = true;
            }

            m_actionData.output = 
                new OutputGameDataBuilder()
                    .WithAxes(axes)
                    .WithKick(kickButton)
                    .Build();

            return TaskStatus.Running;
        }
    }
}