using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class SeekMyGoal : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
            m_actionData.GetDistanceAvoidingBall(m_actionData.input.myGoal.position.ToVector2(), out var targetDistance, out var targetDirection);
            var axes = targetDirection.normalized;
            var dashButton = targetDistance.sqrMagnitude > (m_actionData.input.m_DashDistance * m_actionData.input.m_DashDistance);

            m_actionData.output = 
                new OutputGameDataBuilder()
                    .WithAxes(axes)
                    .WithDash(dashButton)
                    .Build();
            
            return TaskStatus.Running;
        }
    }
}