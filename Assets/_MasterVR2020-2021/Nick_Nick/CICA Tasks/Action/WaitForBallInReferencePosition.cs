using System;
using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
	public class WaitForBallInReferencePosition : CicaBaseAction
	{
		protected override TaskStatus Execute()
		{
			Vector2 strikerPosition = m_actionData.input.referencePos;
			strikerPosition.x = m_actionData.input.midfield.x -
			                    (m_actionData.input.referencePos.x - m_actionData.input.midfield.x);

			m_actionData.output = 
				new OutputGameDataBuilder()
					.WithAxes(m_actionData.Seek(strikerPosition, m_actionData.input.colliderRadius))
					.Build();
			return TaskStatus.Running;
		}
	}
}