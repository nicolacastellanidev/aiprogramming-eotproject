using BehaviorDesigner.Runtime.Tasks;
using Nick_Nick.CICA_SharedVariables;
using UnityEngine;

namespace Nick_Nick.CICA_Tasks.Action
{
    public class DefensiveCharge : CicaBaseAction
    {
        protected override TaskStatus Execute()
        {
           Vector2 axes;
           var kickButton = false;
           var dashButton = false;

            var target = m_actionData.input.ballPosition.ToVector2();
            var ballToMyGoalDir = m_actionData.input.myGoal.position.ToVector2() - target;
            ballToMyGoalDir.Normalize();

            var offset = m_actionData.input.ballRadius;
            offset += m_actionData.input.colliderRadius;
            offset += 0.05f; // Tolerance threshold.

            target += ballToMyGoalDir * offset;
            target = m_actionData.ClampPosition(target, 0.2f);
            axes = m_actionData.Seek(target);
            
            var targetDistance = Vector2.Distance(target, m_actionData.input.myPosition);
            
            if (m_actionData.timers.recoverTimer > m_actionData.input.m_RecoverTimeThreshold)
            {
                kickButton = true; // Force kick to recover.
            }
            else {
                if (targetDistance > m_actionData.input.m_DashDistance)
                {
                    dashButton = true;
                }
                else if (targetDistance <= m_actionData.input.m_KickPrecision)
                {
                   kickButton = true;
                }
            }

            m_actionData.output = 
                new OutputGameDataBuilder()
                    .WithAxes(axes)
                    .WithKick(kickButton)
                    .WithDash(dashButton)
                    .Build();
            
            return TaskStatus.Running;
        }
    }
}