using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace Nick_Nick.CICA_SharedVariables
{
	
	[System.Serializable]
	public class InputGameData
	{
		
		// GAME LOOP
		public float frameTime; //DYNAMIC

		public Vector2 myPosition; //DYNAMIC
		
		public bool isEnergyEnoughToKick = false; //DYNAMIC
		public bool isEnergyEnoughToDash = false; //DYNAMIC
		public bool isEnergyEnoughToTackle = false; //DYNAMIC
		public bool isEnergyEnoughToAttract = false; //DYNAMIC
		
		public Transform ball;
		public float ballDistance; //DYNAMIC
		public float ballRadius;
		public Vector3 ballPosition;
		
		public int teamCharactersCount;
		public int teammatesCount;
		public int opponentsCount;
		
		public Transform myGoal;
		public Transform opponentGoal;

		public Vector2 referencePos; //DYNAMIC
		
		public Vector2 topLeft; //DYNAMIC
		public Vector2 topRight; //DYNAMIC
		public Vector2 bottomLeft; //DYNAMIC
		public Vector2 bottomRight; //DYNAMIC

		public Vector2 midfield; //DYNAMIC
		public float fieldWidth;
		public float fieldHeight;

		public float gkAreaMinHeight;
		public float gkAreaMaxHeight;
		public float gkAreaWidth;
		public float gkAreaHeight;

		public float goalMinHeight;
		public float goalMaxHeight;
		public float goalWidth;
		public float colliderRadius;
		
		// Seek-and-flee behaviour.

		public float m_MinFleeDistanceFactor = 0.25f;
		public float m_MaxFleeDistanceFactor = 0.50f;

		// Separation.

		public float m_SeparationThreshold = 3f;

		// Energy thresholds.

		public float m_MinDashEnergy = 0.40f;
		public float m_MinKickEnergy = 0.05f;
		public float m_MinTackleEnergy = 0.50f;
		public float m_MinAttractEnergy = 0.10f;

		// Cooldown timers.

		public float m_DashCooldown = 0.50f;
		public float m_KickCooldown = 0.25f;
		public float m_TackleCooldown = 2.0f;
		public float m_AttractCooldown = 0.5f;

		// Dash behaviour.

		public float m_DashDistance = 3.5f;
		public float m_ForcedDashDistance = 2f;

		// Kick behaviour.

		public float m_KickPrecision = 0.1f;

		// Tackle behaviour.

		public float m_TackleRadius = 0.8f;
		public float m_BallDistanceThreshold = 2f;

		// Attract behaviour.

		public float m_AttractMinRadius = 0.70f;
		public float m_AttractMaxRadius = 0.95f;

		public float m_AttractTimeThreshold = 2f;

		// Extra parameters.

		public float m_RecoverRadius = 1.0f;
		public float m_RecoverTimeThreshold = 1.0f;

		public List<Transform> m_Opponents;
		public List<Transform> m_Teams;
	}

	[System.Serializable]
	public class SharedInputGameData : SharedVariable<InputGameData>
	{
		public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
		public static implicit operator SharedInputGameData(InputGameData value) { return new SharedInputGameData { mValue = value }; }
	}
}