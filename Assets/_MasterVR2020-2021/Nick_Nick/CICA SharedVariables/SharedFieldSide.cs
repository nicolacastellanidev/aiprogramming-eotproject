using BehaviorDesigner.Runtime;
using UnityEngine;

namespace Nick_Nick.CICA_SharedVariables
{
	[System.Serializable]
	public enum FieldSide
	{
		Left,
		Right
	}

	[System.Serializable]
	public class SharedFieldSide : SharedVariable<FieldSide>
	{
		public override string ToString() { return mValue.ToString(); }
		public static implicit operator SharedFieldSide(FieldSide value) { return new SharedFieldSide { mValue = value }; }
	}
}