using BehaviorDesigner.Runtime;
using UnityEngine;

namespace Nick_Nick.CICA_SharedVariables
{
	[System.Serializable]
	public class OutputGameData
	{
		public Vector2 axes;
		
		public bool tackleRequested;
		public bool kickButton;
		public bool dashButton;
		public bool tauntButton;
		public bool isAttracting;
	}

	[System.Serializable]
	public class SharedOutputGameData : SharedVariable<OutputGameData>
	{
		public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
		public static implicit operator SharedOutputGameData(OutputGameData value) { return new SharedOutputGameData { mValue = value }; }
	}

	public class OutputGameDataBuilder
	{
		private OutputGameData _data = new OutputGameData();

		public static OutputGameDataBuilder Builder()
		{
			return new OutputGameDataBuilder();
		}
		
		public OutputGameDataBuilder WithAxes(Vector2 axes)
		{
			_data.axes = axes;
			return this;
		}
		
		public OutputGameDataBuilder WithTackle(bool tackle)
		{
			_data.tackleRequested = tackle;
			return this;
		}
		
		public OutputGameDataBuilder WithKick(bool kick)
		{
			_data.kickButton = kick;
			return this;
		}
		
		public OutputGameDataBuilder WithDash(bool dash)
		{
			_data.dashButton = dash;
			return this;
		}
		
		public OutputGameDataBuilder WithTaunt(bool taunt)
		{
			_data.tauntButton = taunt;
			return this;
		}
		
		public OutputGameDataBuilder WithAttract(bool attract)
		{
			_data.isAttracting = attract;
			return this;
		}

		public OutputGameData Build()
		{
			return _data;
		}
	}
}