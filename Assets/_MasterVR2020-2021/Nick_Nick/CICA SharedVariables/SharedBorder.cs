using BehaviorDesigner.Runtime;
using UnityEngine;

namespace Nick_Nick.CICA_SharedVariables
{
	[System.Serializable]
	public enum Border
	{
		Top,
		Bottom, 
		Left,
		Right
	}

	[System.Serializable]
	public class SharedBorder : SharedVariable<Border>
	{
		public override string ToString() { return mValue.ToString(); }
		public static implicit operator SharedBorder(Border value) { return new SharedBorder { mValue = value }; }
	}
}