using System;
using BehaviorDesigner.Runtime;

namespace Nick_Nick.CICA_SharedVariables
{
	[Serializable]
	public class GameTimers
	{
		public float kickCooldownTimer = 0f;
		public float dashCooldownTimer = 0f;
		public float tackleCooldownTimer = 0f;
		public float attractCooldownTimer = 0f;
		
		public float recoverTimer = 0f;
		public float attractTimer = 0f;
	}

	[Serializable]
	public class SharedGameTimers : SharedVariable<GameTimers>
	{
		public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
		public static implicit operator SharedGameTimers(GameTimers value) { return new SharedGameTimers { mValue = value }; }
	}
}