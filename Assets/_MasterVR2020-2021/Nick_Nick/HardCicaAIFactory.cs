﻿using UnityEngine;

namespace Nick_Nick
{
    public class HardCicaAIFactory : CicaAiFactory
    {
        public HardCicaAIFactory() : base(s_Params)
        {
            s_Params = "Data/AI/AIParams_Hard";
        }
    }
}