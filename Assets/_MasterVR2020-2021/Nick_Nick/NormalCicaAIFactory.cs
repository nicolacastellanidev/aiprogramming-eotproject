﻿using UnityEngine;

namespace Nick_Nick
{
    public class NormalCicaAIFactory : CicaAiFactory
    {
        public NormalCicaAIFactory() : base(s_Params)
        {
            s_Params = "Data/AI/AIParams_Normal";
        }
    }
}